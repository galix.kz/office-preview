import React, {useEffect, useState} from 'react';
import mammoth from 'mammoth';

const WordToHtmlConverter = ({src}) => {
    const [htmlContent, setHtmlContent] = useState('');
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        const convertToHtml = async () => {
            try {
                setLoading(true);
                const response = await fetch(src);
                const arrayBuffer = await response.arrayBuffer();

                const options = {
                    includeDefaultStyleMap: false
                };
                const result = await mammoth.convertToHtml({arrayBuffer}, options)

                setHtmlContent(result.value);
                setLoading(false);
            } catch (error) {
                console.error('Error converting Word to HTML:', error);
            }
        };

        convertToHtml();
    }, [src]);

    if(loading){
        return <div>Загрузка word...</div>
    }
    return (
        <div className="word__container">
            <div className="word" dangerouslySetInnerHTML={{ __html: htmlContent }} />
        </div>
    );
};

export default WordToHtmlConverter;
