import React, { useEffect, useState } from 'react';
import Handsontable from 'handsontable';
import 'handsontable/dist/handsontable.full.css'; // Import the Handsontable CSS
import * as XLSX from 'xlsx';

function ExcelViewer({src}) {
    const [excelData, setExcelData] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        async function fetchExcelFile() {
            try {
                setLoading(true);
                const response = await fetch(src);

                if (!response.ok) {
                    throw new Error(`HTTP error! Status: ${response.status}`);
                }

                const arrayBuffer = await response.arrayBuffer();
                const data = new Uint8Array(arrayBuffer);
                const workbook = XLSX.read(data, { type: 'array', cellDates: true });
                const sheetName = workbook.SheetNames[0];
                const sheet = workbook.Sheets[sheetName];
                const jsonData = XLSX.utils.sheet_to_json(sheet, {
                    header: 1,
                    raw: false, // This tells the library to perform date conversion,
                    dateNF: 'dd-mm-yyyy', // Date format, adjust as needed
                });
                console.log(jsonData);
                jsonData.forEach(row => {
                    row.forEach((cell, index) => {
                        if (XLSX.SSF.is_date(cell)) {
                            row[index] = XLSX.SSF.parse_date_code(cell).toLocaleDateString();
                        }
                    });
                });

                setLoading(false);
                setExcelData(jsonData);
            } catch (error) {
                console.error('Error fetching Excel file:', error);
            }
        }

        fetchExcelFile();
    }, [src]);

    // Create a Handsontable instance when the component mounts
    useEffect(() => {
        const container = document.getElementById('handsontable-container');

        if (container) {
            const hot = new Handsontable(container, {
                licenseKey: 'non-commercial-and-evaluation',
                data: excelData,
                colHeaders: true,
                rowHeaders: true,
                stretchH: 'all',
                readOnly: true, // Set this to true for read-only mode
                wordWrap: false, // Prevent cell content from wrapping

            });
        }
    }, [excelData]);
    if(loading){
        return <div>Загрузка...</div>
    }
    return (
        <div>
            <div id="handsontable-container"></div>
        </div>
    );
}

export default ExcelViewer;
