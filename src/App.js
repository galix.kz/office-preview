import React, { useEffect, useState } from 'react';
import {getSourceComponent} from "./shared/functions/getSourceComponent";

function MyComponent() {
    const [isLoaded, setIsLoaded] = useState(false);
    const [queryParams, setQueryParams] = useState({});

    useEffect(() => {

        // Get the current URL
        const currentURL = window.location.href.split('?')[1];

        // Create a URLSearchParams object to parse the query parameters
        const urlSearchParams = new URLSearchParams(currentURL);

        // Convert the URLSearchParams object into a plain JavaScript object
        const queryParamsObject = {};
        for (const [key, value] of urlSearchParams.entries()) {
            queryParamsObject[key] = value;
        }

        // Set the query parameters in the component state
        setQueryParams(queryParamsObject);
        setIsLoaded(prev => !prev);

    }, []);
    // console.log(queryParams)

    return (
        <div>
            {!queryParams.src && !queryParams.type && <li>type or src query parameters is missing</li>}
            {getSourceComponent(queryParams)}
        </div>
    );
}

export default MyComponent;
