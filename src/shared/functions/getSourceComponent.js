import WordToHtmlConverter from "../../previewers/word";
import ExcelViewer from "../../previewers/excel";
import {DocumentType} from "../const/document";

export function getSourceComponent(queryParams) {
    const {
        type=DocumentType.XLSX,
        src,
    } = queryParams;
    console.log(type);
    switch (type) {
        case DocumentType.DOCX:
            return <WordToHtmlConverter src={src} />
        case DocumentType.XLSX:
            return <ExcelViewer src={src} />
        default:
            return null;
    }
}
